package ro.orange;

public class EmployeeCounts extends Thread {
    private Integer a, b, bill;

    // constructor with 3 input parameters
    EmployeeCounts(Integer a, Integer b, Integer bill) {
        this.a = a;
        this.b = b;
        this.bill = bill;
    }
    // run method
    public void run() {
        for(int i = this.a; i <= this.b; i += this.bill) {
            System.out.print(this.a + " ");
            this.a += this.bill;
        }
    }
}
